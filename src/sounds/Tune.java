package sounds;

import java.util.ArrayList;
import java.util.Collections;

public class Tune {
	private ArrayList<SoundName> tune;
	
	/**
	 * Makes a standard or drop tune for passed sound.
	 * @param root is a sounnd which determinates the name of tune. 
	 *             For example "SoundName.E" for E standard tune.
	 * @param drop is an information if tune has to be in drop.
	 */
	public Tune(SoundName root,boolean drop)
	{
		tune = new ArrayList<SoundName>();
		tune.add(root);
		int buf;
		if(drop)buf = root.getValue()+7;
		else buf = root.getValue()+5;
		
		tune.add(SoundName.getName(buf));
		buf += 5;
		tune.add(SoundName.getName(buf));
		buf += 5;
		tune.add(SoundName.getName(buf));
		buf += 4;
		tune.add(SoundName.getName(buf));
		buf += 5;
		tune.add(SoundName.getName(buf));
		Collections.reverse(tune);
	}
	/**
	 * Makes a standard or drop tune for passed sound an number of strings.
	 * @param root is a sounnd which determinates the name of tune. 
	 *             For example "SoundName.E" for E standard tune.
	 * @param numOfStrins is a number of strings
	 * @param drop is an information if tune has to be in drop.
	 */
	public Tune(SoundName root, int numOfStrings, boolean drop)
	{
		tune = new ArrayList<SoundName>();
		tune.add(root);
		
		int buf;
		
		if(drop)buf = root.getValue()+7;
		else buf = root.getValue()+5;
		tune.add(SoundName.getName(buf));
		numOfStrings -= 4;
		while(numOfStrings > 0)
		{
			buf += 5;
			tune.add(SoundName.getName(buf));
			numOfStrings--;
		}
		buf += 4;
		tune.add(SoundName.getName(buf));
		buf += 5;
		tune.add(SoundName.getName(buf));
		Collections.reverse(tune);
	}
	
	public SoundName get(int choosen)
	{
		return tune.get(choosen);
	}
	
	public int size()
	{
		return tune.size();
	}

}
