package sounds;

public enum Scales {
	MAJOR("Major/Ionian",
			new Interval[]{
			Interval.UNISON,
			Interval.MAJOR2,
			Interval.MAJOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MAJOR6,
			Interval.MAJOR7}),
	MINOR("Minor/Aeolian",
			new Interval[]{
			Interval.UNISON,
			Interval.MAJOR2,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MINOR6,
			Interval.MINOR7}),
	DORIAN("Dorian",
			new Interval[]{
			Interval.UNISON,
			Interval.MAJOR2,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MAJOR6,
			Interval.MINOR7}),
	PHYRGIAN("Phyrgian",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR2,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MINOR6,
			Interval.MINOR7}),
	LYDIAN("Lydian",
			new Interval[]{
			Interval.UNISON,
			Interval.MAJOR2,
			Interval.MAJOR3,
			Interval.TRITONE,
			Interval.PERFECT5,
			Interval.MAJOR6,
			Interval.MAJOR7}),
	MIXOLYDIAN("Mixolydian",
			new Interval[]{
			Interval.UNISON,
			Interval.MAJOR2,
			Interval.MAJOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MAJOR6,
			Interval.MINOR7}),
	LOCRIAN("Locrian",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR2,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.TRITONE,
			Interval.MINOR6,
			Interval.MINOR7}),
	MOHAMEDDIAN("Mohameddian",
			new Interval[]{
			Interval.UNISON,
			Interval.MAJOR2,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MINOR6,
			Interval.MAJOR7}),
	ADONAIMALAKH("Adonai Malakh",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR2,
			Interval.MAJOR2,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MAJOR6,
			Interval.MINOR7}),
	AHABARABBA("Ahaba Rabba",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR2,
			Interval.MAJOR3,
			Interval.PERFECT4,
			Interval.PERFECT5,
			Interval.MINOR6,
			Interval.MINOR7}),
	MAGENABOT("Magen Abot",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR2,
			Interval.MINOR3,
			Interval.MAJOR3,
			Interval.TRITONE,
			Interval.MINOR6,
			Interval.MINOR7,
			Interval.MAJOR7}),
	BLUES1("Blues 1",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR3,
			Interval.PERFECT4,
			Interval.TRITONE,
			Interval.PERFECT5,
			Interval.MINOR7,
			Interval.MAJOR7}),
	BLUES2("Blues 2",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR3,
			Interval.MAJOR3,
			Interval.PERFECT4,
			Interval.TRITONE,
			Interval.PERFECT5,
			Interval.MINOR7,
			Interval.MAJOR7}),
	BLUES3("Blues 3",
			new Interval[]{
			Interval.UNISON,
			Interval.MINOR3,
			Interval.MAJOR3,
			Interval.PERFECT4,
			Interval.TRITONE,
			Interval.PERFECT5,
			Interval.MAJOR6,
			Interval.MINOR7,
			Interval.MAJOR7});
	
	private final Interval[] intervals;
	private final String name;
	
	Scales(String name, Interval[] intervals)
	{
		this.intervals = intervals;
		this.name=name;
	}

	/**
	 * @return the intervals
	 */
	public Interval[] getIntervals() {
		return intervals;
	}

	/**
	 * @return the name
	 */
	public String toString() {
		return name;
	}

}
