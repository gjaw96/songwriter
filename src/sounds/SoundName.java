package sounds;
/**
 * Represents sounds
 * @author Grzegorz Jaworski
 *
 */
public enum SoundName 
{
	C (0,"C"),
	CS (1,"C#"),
	D (2,"D"),
	DS (3,"D#"),
	E (4,"E"),
	F (5,"F"),
	FS (6,"F#"),
	G (7,"G"),
	GS (8,"G#"),
	A (9,"A"),
	AS (10,"A#"),
	B (11,"B");
	
	private final int value;
	private final String str;
	
	SoundName(int value, String str)
	{
		this.value = value;
		this.str = str;
	}
	
	public int getValue() {
		return value;
	}
	
	public String toString() {
		return str;
	}
	
	static public String toString(int searched)
	{
		return SoundName.getName(searched).toString();
	}
	
	static public SoundName getName(int searched)
	{
		searched = searched % 12;
		switch(searched)
		{
		case 0: return SoundName.C;
		case 1: return SoundName.CS;
		case 2: return SoundName.D;
		case 3: return SoundName.DS;
		case 4: return SoundName.E;
		case 5: return SoundName.F;
		case 6: return SoundName.FS;
		case 7: return SoundName.G;
		case 8: return SoundName.GS;
		case 9: return SoundName.A;
		case 10: return SoundName.AS;
		case 11: return SoundName.B;
		}
		return null;
	}
	
	static public SoundName getName(String searched) throws IllegalArgumentException
	{
		if(searched.length()>2) throw new IllegalArgumentException(
				"Has to be the name of the specific sound");
		switch(searched)
		{
		case "C": return SoundName.C;
		case "C#": return SoundName.CS;
		case "D": return SoundName.D;
		case "D#": return SoundName.DS;
		case "E": return SoundName.E;
		case "F": return SoundName.F;
		case "F#": return SoundName.FS;
		case "G": return SoundName.G;
		case "G#": return SoundName.GS;
		case "A": return SoundName.A;
		case "A#": return SoundName.AS;
		case "B": return SoundName.B;
		default: throw new IllegalArgumentException(
				"Has to be the name of the specific sound");
		}
	}
	
	public SoundName findInterval(Interval interval)
	{
		int buf = this.value;
		buf += interval.getValue();
		
		return SoundName.getName(buf);
	}
}