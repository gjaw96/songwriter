package sounds;

import static org.junit.Assert.*;

import org.junit.Test;

public class TuneTest {

	@Test
	public void testStandardTune() {
		Tune tester = new Tune(SoundName.E,false);
		assertEquals(SoundName.E,tester.get(0));
		assertEquals(SoundName.B,tester.get(1));
		assertEquals(SoundName.G,tester.get(2));
		assertEquals(SoundName.D,tester.get(3));
		assertEquals(SoundName.A,tester.get(4));
		assertEquals(SoundName.E,tester.get(5));
	}
	
	@Test
	public void testDropTune() {
		Tune tester = new Tune(SoundName.D,true);
		assertEquals(SoundName.E,tester.get(0));
		assertEquals(SoundName.B,tester.get(1));
		assertEquals(SoundName.G,tester.get(2));
		assertEquals(SoundName.D,tester.get(3));
		assertEquals(SoundName.A,tester.get(4));
		assertEquals(SoundName.D,tester.get(5));
	}
	@Test
	public void testStandard7StringTune() {
		Tune tester = new Tune(SoundName.B,7,false);
		assertEquals(SoundName.E,tester.get(0));
		assertEquals(SoundName.B,tester.get(1));
		assertEquals(SoundName.G,tester.get(2));
		assertEquals(SoundName.D,tester.get(3));
		assertEquals(SoundName.A,tester.get(4));
		assertEquals(SoundName.E,tester.get(5));
		assertEquals(SoundName.B,tester.get(6));
	}

}
