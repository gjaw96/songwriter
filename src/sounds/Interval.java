package sounds;
/**
 * Represents a music interval
 * @author Grzegorz Jaworski
 *
 */
public enum Interval 
{
	UNISON(0,true),
	MINOR2(1,false),
	MAJOR2(2,false),
	MINOR3(3,true),
	MAJOR3(4,true),
	PERFECT4(5,true),
	TRITONE(6,false),
	PERFECT5(7,true),
	MINOR6(8,true),
	MAJOR6(9,true),
	MINOR7(10,false),
	MAJOR7(11,false),
	OCTAVE(12,true);
	
	private final int value;
	private final boolean consonance;
	
	Interval(int val, boolean con)
	{
		this.value = val;
		this.consonance = con;
	}
	/**
	 * 
	 * @return distance from unison in halfsteps
	 */
	public int getValue()
	{
		return this.value;
	}
	
	public boolean isConsonace()
	{
		return this.consonance;
	}
	
	static public Interval getInterval(int searched)
	{
		searched = searched % 13;
		switch(searched)
		{
		case 0: return Interval.UNISON;
		case 1: return Interval.MINOR2;
		case 2: return Interval.MAJOR2;
		case 3: return Interval.MINOR3;
		case 4: return Interval.MAJOR3;
		case 5: return Interval.PERFECT4;
		case 6: return Interval.TRITONE;
		case 7: return Interval.PERFECT5;
		case 8: return Interval.MINOR6;
		case 9: return Interval.MAJOR6;
		case 10: return Interval.MINOR7;
		case 11: return Interval.MAJOR7;
		case 12: return Interval.OCTAVE;
		}
		return null;
	}
	
	static public Interval findInterval(SoundName uni, SoundName interval)
	{
		int buf = interval.getValue() - uni.getValue();
		while(buf < 0)
		{
			buf += 12;
		}
		return Interval.getInterval(buf);
	}
}
