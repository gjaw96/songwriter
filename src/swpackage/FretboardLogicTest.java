package swpackage;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import sounds.Scales;
import sounds.SoundName;
import sounds.Tune;

public class FretboardLogicTest {

	@Test
	public void testFretboardLogicInit() {
		Tune tune = new Tune(SoundName.E,false);
		FretboardLogic tester = new FretboardLogic(tune,21);
		assertEquals(SoundName.E,tester.getOpen(0));
		assertEquals(SoundName.B,tester.getOpen(1));
		assertEquals(SoundName.G,tester.getOpen(2));
		assertEquals(SoundName.D,tester.getOpen(3));
		assertEquals(SoundName.A,tester.getOpen(4));
		assertEquals(SoundName.E,tester.getOpen(5));
	}
	@Test
	public void testSettingScale()
	{
		Tune tune = new Tune(SoundName.E,false);
		FretboardLogic tester = new FretboardLogic(tune,21);
		tester.setScale(SoundName.E, Scales.BLUES3);
		assertTrue(tester.isInScale(5, 0));
		tester.setScale(SoundName.C, Scales.MAJOR);
		assertFalse(tester.isInScale(1, 14));
		ArrayList<SoundName> buf = new ArrayList<SoundName>();
		buf.addAll(tester.getHighlighted());
		assertEquals(SoundName.C,buf.get(0));
		assertEquals(SoundName.D,buf.get(1));
		assertEquals(SoundName.E,buf.get(2));
		assertEquals(SoundName.F,buf.get(3));
		assertEquals(SoundName.G,buf.get(4));
		assertEquals(SoundName.A,buf.get(5));
		assertEquals(SoundName.B,buf.get(6));
	}

}
