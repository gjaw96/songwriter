package swpackage;

import static org.junit.Assert.*;

import org.junit.Before;

import sounds.SoundName;

import org.junit.Test;

public class TestGuitarString {
	GuitarString tester;

	@Before
	public void testBuild()
	{
		 this.tester = new GuitarString.Builder(SoundName.E)
								.frets((int)24)
								.build();
	}
	@Test
	public void testGetRoot() {
		assertEquals(SoundName.E,tester.getRoot());
	}

	@Test
	public void testGetFrets() {
		assertEquals(24,tester.getFrets());
	}

	@Test
	public void testRetune() {
		this.tester.retune(SoundName.D);
		assertEquals(SoundName.D,tester.getRoot());
		this.tester.retune(SoundName.E);
		assertEquals(SoundName.E,tester.getRoot());
	}

	@Test
	public void testChangeNumberOfFrets() {
		try {
			this.tester.changeNumberOfFrets(22);
		} catch (Exception e) {
			fail(e.toString());
		}
		assertEquals(22,this.tester.getFrets());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testNumberOfFretsShouldBeMoreThanOne()
	{
		this.tester.changeNumberOfFrets(0);
	}

	@Test
	public void testSoundOnFret() {
		assertEquals(this.tester.getRoot(),this.tester.soundOnFret(12));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSearchedFretShouldNotBeLessThanZero()
	{
		this.tester.soundOnFret(-1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSearchedFretShouldBeNoMoreThanNuberOfFrets()
	{
		this.tester.soundOnFret(this.tester.getFrets()+1);
	}
}
