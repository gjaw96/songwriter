package swpackage;

import java.util.ArrayList;

import sounds.Interval;
import sounds.Scales;
import sounds.SoundName;
import sounds.Tune;

public class FretboardLogic {
	private ArrayList<GuitarString> strings;
	private ArrayList<SoundName> highlighted;

	FretboardLogic(Tune tune, int frets)
	{
		strings = new ArrayList<GuitarString>();
		setHighlighted(new ArrayList<SoundName>());
		
		for(int i = 0; i < tune.size();i++)
		{
			GuitarString bufString = new GuitarString
					.Builder(tune.get(i))
					.frets(frets)
					.build();
			strings.add(bufString);
		}
	}
	/**
	 * 
	 * @param string is a number of requested string from bottom of fretboard
	 * starting at 0.
	 * @return returns a name of the sound on the open string
	 */
	public SoundName getOpen(int string)
	{
		return strings
				.get(string)
				.getRoot();
	}
	/**
	 * Gives the sound on requested fret
	 * @param string is a number of string from bottom of fretoard starting at 0
	 * @param fret is a number of fret
	 * @return name of the sound on the fret given as argument
	 */
	public SoundName getFret(int string, int fret)
	{
		return strings
				.get(string)
				.soundOnFret(fret);
	}
	/**
	 * Tells if searched sound is on a given fret on a given string
	 * @param searched is name of searched sound
	 * @param string is a number of requested string from bottom of fretboard
	 * starting at 0.
	 * @param fret is a number of fret
	 * @return information if searched sound is on a given fret on a given string
	 */
	public boolean isSearched(SoundName searched, int string, int fret)
	{
		SoundName buf = strings.get(string).soundOnFret(fret);
		return searched == buf;
	}
	/**
	 * Makes list of sounds that will be highlighted
	 * @param root is a root sound of a scale
	 * @param scale is a type of a scale
	 */
	public void setScale(SoundName root, Scales scale)
	{

		//Clear ArrayList if not empty
		if(!this.highlighted.isEmpty())
		{
			this.highlighted.clear();
		}
		//Fill it with sounds
		Interval[] buf = scale.getIntervals();
		for(Interval interval: buf)
		{
			this.highlighted.add(root.findInterval(interval));
		}
	}
	/**
	 * Checks if the given sound is in the currently set scale
	 * @param string is a number of requested string from bottom of fretboard
	 * starting at 0.
	 * @param fret is a number of fret
	 * @return information if the given sound is in a currently set scale
	 */
	public boolean isInScale(int string, int fret)
	{
		boolean answer = false;
		if(this.getHighlighted().isEmpty()) return answer;
		
		for(SoundName sound: this.getHighlighted())
		{
			answer = this.isSearched(sound, string, fret);
			if(answer) break;
		}
		
		return answer;
	}
	
	public boolean isUnison(int string, int fret)
	{
		if(this.getHighlighted().isEmpty()) return false;
		return isSearched(highlighted.get(0),string,fret);
	}
	/**
	 * @return the highlighted
	 */
	public ArrayList<SoundName> getHighlighted() {
		return highlighted;
	}
	/**
	 * @param highlighted the list of sounds to set as highlighted 
	 */
	public void setHighlighted(ArrayList<SoundName> highlighted) {
		this.highlighted = highlighted;
	}
	/**
	 * clears list of selected notes
	 */
	public void clearHighlights()
	{
		this.highlighted.clear();
	}
	/**
	 * Highlights notes from the list
	 * @param list
	 */
	public void highlightSelected(SoundName[] list)
	{
		this.highlighted.clear();
		for(SoundName sound: list)
		{
			highlighted.add(sound);
		}
	}
}
