package swpackage;

import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import sounds.Scales;
import sounds.SoundName;
import sounds.Tune;
/**
 * 
 * @author Grzegorz Jaworski
 *
 */
public class Fretboard extends Canvas 
{
	/**
	 * Contains image of the note on the fretboard
	 * @author Grzegorz Jaworski
	 *
	 */
	private class Note
	{
		private Image img;
		Note(Image i)
		{
			img = i;
		}
		public Image getImage()
		{
			return img;
		}
		public void changeImage(Image newImage)
		{
			img = newImage;
		}
	}
	/**
	 * is a Builder for fretboard class
	 * @author Grzegorz Jaworski
	 *
	 */
	public static class Builder
	{
		private SoundName root;
		private boolean drop;
		private int imgSize;
		private int frets;
		private int strings;
		private int[] offsets;
		
		/**
		 * 
		 * @param rt is a root note of the guitar tuning
		 * @param drp is information if it is drop tuning
		 */
		public Builder(SoundName rt, boolean drp)
		{
			this.root = rt;
			this.drop = drp;
		}
		/**
		 * make your custom offsets
		 * @param top
		 * @param left
		 * @param bottom
		 * @param right
		 * @return
		 */
		public Builder setOffsets(int top, int left, int bottom, int right)
		{
			offsets = new int[4];
			offsets[0] = top;
			offsets[1] = left;
			offsets[2] = bottom;
			offsets[3] = right;
			return this;
		}
		/**
		 * set all offsets to 5 px
		 * @return
		 */
		public Builder setOffsets()
		{
			offsets = new int[]{5,5,5,5};
			return this;
		}
		
		public Builder frets(int frts)
		{
			this.frets = frts;
			return this;
		}
		public Builder strings(int strngs)
		{
			this.strings = strngs;
			return this;
		}
		public Builder imageSize(int size)
		{
			this.imgSize = size;
			return this;
		}
		
		public Fretboard build()
		{
			return new Fretboard(this);
		}
		
	}
	private final int leftOffset;
	private final int topOffset;
	private final int bottomOffset;
	private final int rightOffset;
	private int imgSize;
	private ArrayList<ArrayList<Note>> notes;
	private Image emptyNoteImg;
	private Image selectedNoteImg;
	private Image unisonNoteImg;
	private FretboardLogic logic;
	private Popup popup;
	
	
	/**
	 * @param build is a builder of the Fretboard
	 */
	private Fretboard(Builder build)
	{
		super(build.frets*build.imgSize+(build.imgSize/2)+build.offsets[1]+build.offsets[3],
				build.strings*build.imgSize+(build.imgSize/2)+build.offsets[0]+build.offsets[2]);
		imgSize = build.imgSize;
		topOffset = build.offsets[0];
		leftOffset = build.offsets[1];
		bottomOffset = build.offsets[2];
		rightOffset = build.offsets[3];
		
		loadImages();
		
		notes = new ArrayList<ArrayList<Note>>();
		logic = new FretboardLogic(new Tune(build.root,build.strings,build.drop), build.frets);
		
		fillNotesList(build);
		
		this.addEventHandler(MouseEvent.MOUSE_CLICKED, 
				new EventHandler<MouseEvent>()
				{
			@Override
			public void handle(MouseEvent click)
			{
				processClick(click);
			}
				});
	}
	/**
	 * Loads images of notes
	 */
	private void loadImages() {
		emptyNoteImg = new Image("file:res/emptynote.png",
				this.imgSize,this.imgSize,false, false);
		selectedNoteImg = new Image("file:res/selectednote.png",
				this.imgSize,this.imgSize,false,false);
		unisonNoteImg = new Image("file:res/unison.png",
				this.imgSize,this.imgSize,false,false);
	}
	/**
	 * fills list of notes
	 * @param build is a builder of Fretboard
	 */
	private void fillNotesList(Builder build) {
		for(int i = 0; i < build.strings; i++)
		{
			ArrayList<Note> buf = new ArrayList<Note>();
			for(int j = 0; j < build.frets; j++)
			{
				buf.add(new Note(emptyNoteImg));
			}
			notes.add(buf);
		}
	}
	/**
	 *  draws the whole fretboard
	 */
	public void draw()
	{
		GraphicsContext gc = this.getGraphicsContext2D();
		int fontsize = (this.imgSize*2)/3;
		gc.setFont(Font
				.font("Roboto",fontsize));
		notes.forEach((string) -> {
			//Take name of a sound on an open string
			String open = logic.getOpen(notes.indexOf(string)).toString();

			setColor(gc, string);
			gc.fillText(open, this.leftOffset, this.topOffset + fontsize + this.imgSize*(0.1+notes.indexOf(string)));
			string.forEach((note) -> {
				int fret = string.indexOf(note)+1;
				int currentString = notes.indexOf(string);
				
				//Check if note is highlighted
				if(logic.isInScale(
						currentString,
						fret))
				{
					if(logic.isUnison(
							currentString, 
							fret)) note.changeImage(unisonNoteImg);
					else note.changeImage(selectedNoteImg);
				}
				gc.drawImage(
						note.getImage(),
						this.imgSize*string.indexOf(note)+fontsize+this.leftOffset,
						this.imgSize*notes.indexOf(string)+this.topOffset);
			});
		});
		drawDots(gc,fontsize);
	}
	/**
	 * draws dots at the bottom of fretboard
	 * @param gc
	 * @param font size i a size of a font
	 */
	private void drawDots(GraphicsContext gc, int fontsize) {
		gc.setFill(Color.BLACK);
		//calculating dot size
		double dotSize = 7*this.imgSize/30; 
		//calculating distance from left corner of image to a place to draw a dot
		double fromLeftImgCorner = (this.imgSize - dotSize)/2;
		
		for(int i = 0;i < notes.get(0).size();i++)
		{
			int dot = i%12;
			//calculating distance from left edge of Fretboard
			double leftSpace = this.leftOffset+fontsize+fromLeftImgCorner+i*this.imgSize;
			if(dot!=10 && dot!=0 && dot%2==0)
				gc.fillOval(
						leftSpace,
						notes.size()*this.imgSize+this.topOffset, 
						dotSize, dotSize);
			else if(dot==11)
			{
				gc.fillOval(
						leftSpace,
						notes.size()*this.imgSize+this.topOffset, 
						dotSize,dotSize);
				gc.fillOval(
						leftSpace,
						notes.size()*this.imgSize+dotSize+1+this.topOffset,
						dotSize, dotSize);
			}			
		}
	}
	/**
	 * sets color of the notes at open strings 
	 * (letters at the left side of fretboard)
	 * @param gc
	 * @param string is a list of notes on the string
	 */
	private void setColor(GraphicsContext gc, ArrayList<Note> string) {
		if(logic.isUnison(
				notes.indexOf(string),
				0))gc.setFill(Color.rgb(119, 0, 255));
		else if(logic.isInScale(
				notes.indexOf(string),
				0))gc.setFill(Color.RED);
		else gc.setFill(Color.BLACK);
	}
	/**
	 * sets desirable scale on the fretboard
	 * @param root
	 * @param scale
	 */
	public void setScale(SoundName root, Scales scale)
	{
		logic.setScale(root, scale);
	}
	/**
	 * clears setted scale on the fretboard
	 */
	public void clear()
	{
		logic.clearHighlights();
		for(ArrayList<Note> string: notes)
		{
			for(Note note: string)
			{
				if(!(note.img == emptyNoteImg))
				{
					note.changeImage(emptyNoteImg);
				}
			}
		}
	}
	/**
	 * takes care of user input to fretboard
	 * @param click
	 */
	private void processClick(MouseEvent click) 
	{
		double posx = click.getX();
		//calculate Boundaries
		double leftBoundary = (this.imgSize*2)/3+this.leftOffset;
		double bottomBoundary = this.bottomOffset + (this.imgSize/2)-topOffset;
		if(posx>leftBoundary &&
				posx<(this.getWidth()-this.rightOffset))
		{
			double posy = click.getY();
			if(posy>this.topOffset &&
					posy<(this.getHeight()-bottomBoundary))
			{
				showInfo(leftBoundary, bottomBoundary,click);
			}
		}
	}
	/**
	 * shows popup with a name of sound, fret and string
	 * @param leftBoundary is a distance from left edge of controller to first fret
	 * @param bottomBoundary is a distance from the bottom edge of controller to last string
	 * @param click is a mouse event that occurred
	 */
	private void showInfo(
			double leftBoundary, 
			double bottomBoundary, 
			MouseEvent click) {
		//calculating position related to image of fretboard
		double bufx = click.getX() - leftBoundary;
		double bufy = click.getY() - this.topOffset;
		//calculating string and fret number
		int fret = (int) (bufx/this.imgSize) + 1;
		int string = (int) (bufy/this.imgSize);
		//asking logic which sound is it
		String answer = logic.getFret(string, fret).toString();
		//initiate popup
		if(popup==null)
		{
			popup = new Popup();
			popup.setAutoHide(true);
			popup.setHideOnEscape(true);
		}
		if(popup.isShowing())
		{
			popup.hide();
		}
		VBox layout = popupContent(answer, fret, string);
		layout.setPadding(new Insets(5,5,5,5));
		popup.getContent().add(layout);
		popup.show(this,click.getScreenX(),click.getScreenY());
	}
	/**
	 * Defines content of Popup
	 * @param sound is a string with the name of the sound
	 * @param fret is a number of fret
	 * @param string is a number of string starting at 0
	 * @return VBox with labels and button to close popup
	 */
	private VBox popupContent(String sound, int fret, int string) {
		VBox layout = new VBox(4);
		Button close = new Button("Close");
		close.setOnAction((e) -> {
			this.popup.hide();
		});
		layout.getChildren().addAll(
				new Label(sound),
				new Label("Fret: "+fret),
				new Label("String: "+(string+1)),
				close);
		layout.setBackground(new Background(new BackgroundFill(
			    Color.RED, null, null)));
		return layout;
	}
}
