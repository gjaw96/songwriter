package swpackage;

import sounds.SoundName;
/**
 * represents guitar string on the freboard
 * @author Grzegorz Jaworski
 *
 */
public class GuitarString 
{
	private SoundName rootnote;
	private int frets;
	
	private GuitarString(final Builder builder)
	{
		this.rootnote = builder.rt;
		this.frets = builder.frts;
	}
	/**
	 * builder for guitarstring class
	 * @author Grzegorz Jaworski
	 *
	 */
	public static class Builder
	{
		private final SoundName rt;//Number of sound on the open string
		private int frts;//number of frets
		
		public Builder(final SoundName open)
		{
			this.rt = open;
		}
		
		public Builder frets(final int fretnum)
		{
			this.frts = fretnum;
			return this;
		}
		public GuitarString build()
		{
			return new GuitarString(this);
		}
	}
	/**
	 * 
	 * @return name of the sound at open string
	 */
	public SoundName getRoot()
	{
		return this.rootnote;
	}
	public int getFrets()
	{
		return this.frets;
	}
	/**
	 * Changes tuning of the string
	 * @param tuning
	 */
	protected void retune(final SoundName tuning)
	{
		this.rootnote = tuning;
	}
	/**
	 * Changes number of frets of the fretboard
	 * @param newfrets
	 * @throws IllegalArgumentException
	 */
	protected void changeNumberOfFrets(final int newfrets) throws IllegalArgumentException
	{
		if(newfrets < 1) throw new IllegalArgumentException("There should be at least one fret!");
		this.frets=newfrets;
	}
	/**
	 * 
	 * @param fretnumber
	 * @return name of the sound at given fret
	 * @throws IllegalArgumentException
	 */
	public SoundName soundOnFret(int fretnumber) throws IllegalArgumentException
	{
		if(fretnumber<0) throw new IllegalArgumentException("Fret should be more or equl to 0.");
		else if(fretnumber>this.frets) throw new IllegalArgumentException("There are only " + this.frets + " frets!");
		else return SoundName.getName(rootnote.getValue()+fretnumber);
	}
}
