package swpackage;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sounds.Scales;
import sounds.SoundName;

public class SWriterGUI extends Application {
	//initial properties of fretboard
	private final int initFrets = 24;
	private final int initStrings = 7;
	private final SoundName initTuning = SoundName.B;
	
	@Override
	public void start(Stage window)
	{

		window.setTitle("SongWriter v.0.1.0");
		TabPane root = new TabPane(); 
		Scene scene = new Scene(root);
		makePlusTab(root);
		makeTab(root,initTuning,false,initFrets,initStrings);
		root.getTabs().get(0).setDisable(false);
		window.setScene(scene);
		window.setHeight(500);
		window.setWidth(772);
		window.setResizable(false);
		window.show();
	}

	/**
	 * Makes tab which will be creating new tabs if selected
	 * @param root is a TabPane
	 */
	private void makePlusTab(TabPane root) {
		Tab plusTab = new Tab("+");
		plusTab.setClosable(false);
		plusTab.setOnSelectionChanged((e)->{
			if(plusTab.isSelected())
			{
				if(root.getTabs().size()<2)
				{
					Platform.exit();
				}
				root.getSelectionModel().selectNext();
				GridPane fretboardSettings = fillFretboardSettings(root);
				Stage popupWin = new Stage();
				popupWin.setScene(new Scene(fretboardSettings));
				popupWin.initModality(Modality.APPLICATION_MODAL);
				popupWin.setResizable(false);
				popupWin.show();
			}
		});
		plusTab.setDisable(true);
		root.getTabs().add(plusTab);
	}
	/**
	 * Makes new tab with fretboard
	 * @param pane is a TabPane containing tabs with fretboards
	 * @param tuning
	 * @param drop
	 * @param frets
	 * @param strings
	 */
	private void makeTab(
			TabPane pane,
			SoundName tuning,
			boolean drop,
			int frets,
			int strings)
	{
		//Generate tab name
		String buf;
		if(drop)buf = " drop";
		else buf = " standard";
		String tabName = strings + " string " + frets + " frets " + tuning.toString() + buf;
		//Create tab and add it to the TabPane
		Tab tab = new Tab(tabName);
		pane.getTabs().add(tab);
		//Make content o the tab
		VBox outterBox = new VBox();
		outterBox.setPadding(new Insets(
				5,5,5,5));
		tab.setContent(outterBox);
		tab.setOnCloseRequest((e)->{
			pane.getSelectionModel().selectNext();
		});
		Fretboard fretboard =  new Fretboard.Builder(tuning,drop)
				.frets(frets)
				.imageSize(30)
				.setOffsets()
				.strings(strings)
				.build();
		//Make a TilePane to store fretboard
		TilePane frtbBox = new TilePane();
		frtbBox.getChildren().add(fretboard);
		frtbBox.setAlignment(Pos.CENTER);
		GridPane scaleSettings = fillScaleSettings(fretboard);
		outterBox.getChildren().addAll(frtbBox,scaleSettings);
		
		fretboard.draw();
		pane.getSelectionModel().select(tab);
	}
	
	/**
	 * Makes a GridPane with controlers for setting new scale on the fretboard
	 * @param fretboard
	 * @return
	 */
	private GridPane fillScaleSettings(Fretboard fretboard) 
	{
		GridPane scaleSettings = makeCustomGridPane();
		//HBox with ChoiceBox to choose scale's unison and its label
		Label setRootLabel = makeCustomLabel("Unison:");
		ChoiceBox<SoundName> setRoot = new ChoiceBox<SoundName>(
				FXCollections
				.observableArrayList(
						SoundName.values()));
		//HBox with ChoiceBox to choose scale and its label
		Label setScaleLabel = makeCustomLabel("Scale:");
		ChoiceBox<Scales> setScale = new ChoiceBox<Scales>(
				FXCollections
				.observableArrayList(
						Scales.values()));
		//HBox with buttons to clear and set scale
		Button clearButton = new Button("Clear");
		Button setButton = new Button("Set");	
		scaleSettings.add(setRootLabel, 0, 0);
		scaleSettings.add(setRoot, 1, 0);
		scaleSettings.add(setScaleLabel, 0, 1);
		scaleSettings.add(setScale, 1, 1);
		scaleSettings.add(clearButton, 0, 2);
		scaleSettings.add(setButton, 1,2);
		setDefaultScale(setRoot, setScale);
		setScaleActions(fretboard,setRoot, setScale, clearButton, setButton);
		return scaleSettings;
	}

	/**
	 * Makes label with 30px height
	 * @param labelText
	 * @return
	 */
	private Label makeCustomLabel(String labelText) {
		Label label = new Label(labelText);
		label.setPrefHeight(30);
		return label;
	}
	/**
	 * sets default scale
	 * @param setRoot
	 * @param setScale
	 */
	private void setDefaultScale(
			ChoiceBox<SoundName> setRoot, 
			ChoiceBox<Scales> setScale) 
	{
		//set default settings
		setRoot.getSelectionModel().select(SoundName.E);
		setScale.getSelectionModel().select(Scales.MAJOR);
	}
	/**
	 * contains Event Handlers for controllers within ScaleSetting
	 * @param fretboard is a currently displayed fretboard
	 * @param setRoot is a choicebox to choose unison
	 * @param setScale is a choicebox to choose scale
	 * @param clearButton is a button to clear fretboard
	 * @param setButton is a button to draw scale on the displayed fretboard
	 */
	private void setScaleActions(Fretboard fretboard, ChoiceBox<SoundName> setRoot, ChoiceBox<Scales> setScale,
			Button clearButton, Button setButton) 
	{
		setRoot.setOnAction((e)->{
			changeScale(fretboard,setRoot, setScale);
		});
		
		setScale.setOnAction((e)->{
			changeScale(fretboard,setRoot, setScale);
		});
		
		clearButton.setOnAction((e)->{
			fretboard.clear();
			fretboard.draw();
		});
		
		setButton.setOnAction((e)->{
			changeScale(fretboard,setRoot, setScale);
		});
	}
	/**
	 * Changes scale on the fretboard
	 * @param fretboard
	 * @param setRoot
	 * @param setScale
	 */
	private void changeScale(
			Fretboard fretboard,
			ChoiceBox<SoundName> setRoot,
			ChoiceBox<Scales> setScale) 
	{
		fretboard.clear();
		fretboard.setScale(setRoot.getValue(), setScale.getValue());
		fretboard.draw();
	}
	/**
	 * Makes a GridPane with controlers for creating new fretboard
	 * @return
	 */
	private GridPane fillFretboardSettings(TabPane pane) 
	{
		GridPane fretboardSettings = makeCustomGridPane();
		//HBox with ChoiceBox to choose tuning and its label
		Label setTuningLabel = makeCustomLabel("Tuning:");
		ChoiceBox<SoundName> setTuning= new ChoiceBox<SoundName>(
				FXCollections
				.observableArrayList(
						SoundName.values()));
		Label dropLabel= makeCustomLabel("Drop:");
		CheckBox drop = new CheckBox();
		//HBox with ChoiceBox to choose number of strings and its label
		Label setStringsLabel = makeCustomLabel("Number of Strings:");
		ChoiceBox<Integer> setStrings = new ChoiceBox<Integer>(
				FXCollections
				.observableArrayList(
						6,7,8,9));
		//HBox with ChoiceBox to choose number of frets and its label
		Label setFretsLabel = makeCustomLabel("Number of Strings:");
		ChoiceBox<Integer> setFrets = new ChoiceBox<Integer>(
				FXCollections
				.observableArrayList(
						21,24));
		//Button to set display desirable fretboard
		Button create = new Button("Create");
		fillFretboardSettingsGridPane(fretboardSettings, setTuningLabel, setTuning, dropLabel, drop, setStringsLabel, setStrings,
				setFretsLabel, setFrets, create);
		setDefaultFretboard(setTuning, setStrings, setFrets);
		
		create.setOnAction((e)->{
			makeTab(
					pane, 
					setTuning.getValue(), 
					drop.isSelected(),
					setFrets.getValue(),
					setStrings.getValue()
					);
		});
		return fretboardSettings;
	}

	/**
	 * Makes GridPane with 5px Paddings, 5px gaps and aligment to center
	 * @return
	 */
	private GridPane makeCustomGridPane() {
		GridPane gp = new GridPane();
		gp.setPadding(new Insets(5,5,5,5));
		gp.setHgap(5);
		gp.setVgap(5);
		gp.setAlignment(Pos.CENTER);
		return gp;
	}

	/**
	 * fills GridPane with controlers
	 * @param fretboardSettings
	 * @param setTuningLabel
	 * @param setTuning
	 * @param dropLabel
	 * @param drop
	 * @param setStringsLabel
	 * @param setStrings
	 * @param setFretsLabel
	 * @param setFrets
	 * @param set
	 */
	private void fillFretboardSettingsGridPane(GridPane fretboardSettings, Label setTuningLabel, ChoiceBox<SoundName> setTuning,
			Label dropLabel, CheckBox drop, Label setStringsLabel, ChoiceBox<Integer> setStrings, Label setFretsLabel,
			ChoiceBox<Integer> setFrets, Button set) {
		fretboardSettings.add(setTuningLabel, 0, 0);
		fretboardSettings.add(setTuning, 1, 0);
		fretboardSettings.add(dropLabel, 0, 1);
		fretboardSettings.add(drop, 1, 1);
		fretboardSettings.add(setStringsLabel, 0, 2);
		fretboardSettings.add(setStrings, 1,2);
		fretboardSettings.add(setFretsLabel, 0, 3);
		fretboardSettings.add(setFrets, 1, 3);
		fretboardSettings.add(set, 1, 4);
	}
	/**
	 * Sets default fretboard setting
	 * @param setTuning
	 * @param setStrings
	 */
	private void setDefaultFretboard(
			ChoiceBox<SoundName> setTuning, 
			ChoiceBox<Integer> setStrings, 
			ChoiceBox<Integer> setFrets) 
	{
		setTuning.getSelectionModel().select(initTuning);
		setStrings.getSelectionModel().select((Integer) initStrings);
		setFrets.getSelectionModel().select((Integer) initFrets);
		
	}	
	public static void main(String[] args) 
	{
		launch(args);
	}
}
